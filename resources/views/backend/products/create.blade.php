<x-backend.layout.master>
    <x-slot:title>
        Product Create
    </x-slot:title>

    @push('css')
        <link href="{{ asset('ui/backend') }}/css/sb-admin-2.min.css" rel="stylesheet">
    @endpush

    @push('js')
    @endpush


    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-header py-3">
            <h2>{{ $heading ?? 'Product' }}</h2>
        </div>
        <div class="card-body">
            <i class="fas fa-table me-1 mb-4"></i>
            {{ $title ?? 'Product Create' }}
            <a class="btn btn-sm btn-primary" href="">Create</a>

            <x-backend.alerts.errors />

            <form method="POST" action="{{ route('products.store') }}">
                @csrf

                <x-backend.forms.input name="title" type="text" :value="old('title')" />
                <x-backend.forms.textarea name="description" :value="old('description')" />

                {{-- <select name="" id=""></select> --}}

                <x-backend.forms.button color="primary" text="Create" />

            </form>
        </div>
    </div>

</x-backend.layout.master>
