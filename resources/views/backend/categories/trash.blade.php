<x-backend.layout.master>
    <x-slot:title>
        Categories
    </x-slot:title>

    <x-slot:pageTitle>
        Categories
    </x-slot:pageTitle>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Category Trash List
            <a class="btn btn-sm btn-primary" href="{{route('categories.index') }}">List</a>
        </div>
        <div class="card-body">

            @if ($errors->any())
                <x-backend.alerts.errors />
            @endif
            
            <x-backend.alerts.message type="success" :message="session('message')" />

            <table class="table">
                <thead>
                    <tr>
                        <th>SL#</th>
                        <th>Title</th>
                        <th>Is Active ?</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $category->title }}</td>
                        <td>{{ $category->is_active ? 'Active' : 'In Active' }}</td>
                        <td>
                                        
                            <form method="post" action="{{ route('categories.restore', ['id' => $category->id]) }}" style="display:inline">
                                @csrf
                                @method('patch')
                                <x-backend.forms.button color="warning" onclick="return confirm('Are you sure want to restore?')" text="Restore" />
                            </form>


                            <form method="post" action="{{ route('categories.delete', ['id' => $category->id]) }}" style="display:inline">
                                @csrf
                                @method('delete')
                                <x-backend.forms.button color="danger" onclick="return confirm('Are you sure want to delete permanently?')" text="Delete" />
                            </form>


                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $categories->links() }}

        </div>
    </div>

    {{-- page specific css --}}
    @push('css')

    @endpush

    {{-- page specific js --}}
    @push('js')
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="{{ asset('ui/backend') }}/js/datatables-simple-demo.js"></script>
    @endpush

</x-backend.layout.master>