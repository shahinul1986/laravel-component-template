<x-backend.layout.master>
    <x-slot:title>
        Course Create
    </x-slot:title>

    @push('css')
        <link href="{{ asset('ui/backend') }}/css/sb-admin-2.min.css" rel="stylesheet">
    @endpush

    @push('js')
    @endpush


    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-header py-3">
            <h2>{{ $heading ?? 'Courses' }}</h2>
        </div>
        <div class="card-body">
            <i class="fas fa-table me-1 mb-4"></i>
            {{ $title ?? 'Course Create' }}
            <a class="btn btn-sm btn-primary" href="">Create</a>

            <x-backend.alerts.errors />

            <form method="POST" action="{{ route('courses.store') }}" enctype="multipart/form-data">
                @csrf

                <x-backend.forms.input name="title" type="text" :value="old('title')" />
                <x-backend.forms.input name="batch_no" type="text" :value="old('batch_no')" />
                <x-backend.forms.input name="class_stard_date" type="date" :value="old('class_stard_date')" />
                <x-backend.forms.input name="class_end_date" type="date" :value="old('class_end_date')" />
                <x-backend.forms.input name="instructor_name" type="text" :value="old('instructor_name')" />
                <x-backend.forms.input name="image" type="file"/>
                <x-backend.forms.checkbox class="form-check-input" name="is_active" type="checkbox" :value="1" />
                <x-backend.forms.checkbox class="form-check-input" name="course_type" type="select" :value="1" />

                <x-backend.forms.button color="primary" text="Create" />

            </form>
        </div>
    </div>

</x-backend.layout.master>
