<x-backend.layout.master>
    <x-slot:title>
        Course List
    </x-slot:title>

    @push('css')
        <link href="{{ asset('ui/backend') }}/css/sb-admin-2.min.css" rel="stylesheet">

        <link href="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    @endpush

    @push('js')
        <script src="{{ asset('ui/backend') }}/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <script src="{{ asset('ui/backend') }}/js/demo/datatables-demo.js"></script>
    @endpush

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2>{{ $heading ?? 'Courses' }}</h2>
        </div>
        <div class="card-body">
            <i class="fas fa-table me-1 mb-4"></i> {{ $title ?? 'Course List' }}

            <x-backend.utilities.link-add href="{{ route('courses.create') }}" />
            <x-backend.utilities.link-trash href="{{ route('courses.trash') }}" />


            {{-- Error Message --}}
            <x-backend.alerts.message type="success" :message="session('message')" />

            <div class="table-responsive">
                {{-- <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0"> --}}
                <table id="tbl_course" class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>SL#</th>
                            <th>Title</th>
                            <th>Batch No</th>
                            <th>Class_Start_Date</th>
                            <th>Class_End_Date</th>
                            <th>Instructor Name</th>
                            <th>Is_Active</th>
                            <th>Course_Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($courses as $course)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $course->title }}</td>
                                <td>{{ $course->batch_no }}</td>
                                <td>{{ $course->class_stard_date }}</td>
                                <td>{{ $course->class_end_date }}</td>
                                <td>{{ $course->instructor_name }}</td>
                                <td>{{ $course->is_active ? 'Active' : 'In Active' }}</td>
                                <td>{{ $course->course_type }}</td>
                                <td>

                                    <x-backend.utilities.link-show
                                        href="{{ route('courses.show', ['course' => $course->id]) }}" />
                                    <x-backend.utilities.link-edit
                                        href="{{ route('courses.edit', ['course' => $course->id]) }}" />

                                    <form method="POST"
                                        action="{{ route('courses.destroy', ['course' => $course->id]) }}"style="display:inline">
                                        @csrf
                                        @method('delete')

                                        <x-backend.forms.button color="danger" text="Delete"
                                            onclick="return confirm('Are you sure want to delete')" />
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                {{ $courses->links() }}
            </div>
        </div>
    </div>
@push('js')
<script>
    $("#tbl_course").DataTable({
        responsive: true
    });
</script>
@endpush

</x-backend.layout.master>
