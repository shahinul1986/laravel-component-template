<x-backend.layout.master>
    <x-slot:title>
        Courses
    </x-slot:title>

    <x-slot:pageTitle>
        Courses
    </x-slot:pageTitle>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Courses Trash List
            <a class="btn btn-sm btn-primary" href="{{ route('courses.index') }}">List</a>
        </div>
        <div class="card-body">

            @if ($errors->any())
                <x-backend.alerts.errors />
            @endif

            <x-backend.alerts.message type="success" :message="session('message')" />

            <table class="table">
                <thead>
                    <tr>
                        <th>SL#</th>
                        <th>Title</th>
                        <th>Batch No</th>
                        <th>Class_Start_Date</th>
                        <th>Class_End_Date</th>
                        <th>Instructor Name</th>
                        <th>Is_Active</th>
                        <th>Course_Type</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($courses as $course)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $course->title }}</td>
                            <td>{{ $course->batch_no }}</td>
                            <td>{{ $course->class_stard_date }}</td>
                            <td>{{ $course->class_end_date }}</td>
                            <td>{{ $course->instructor_name }}</td>
                            <td>{{ $course->is_active ? 'Active' : 'In Active' }}</td>
                            <td>{{ $course->course_type }}</td>
                            <td>

                                <form method="post" action="{{ route('courses.restore', ['id' => $course->id]) }}"
                                    style="display:inline">
                                    @csrf
                                    @method('patch')
                                    <x-backend.forms.button color="warning"
                                        onclick="return confirm('Are you sure want to restore?')" text="Restore" />
                                </form>


                                <form method="post" action="{{ route('courses.delete', ['id' => $course->id]) }}"
                                    style="display:inline">
                                    @csrf
                                    @method('delete')
                                    <x-backend.forms.button color="danger"
                                        onclick="return confirm('Are you sure want to delete permanently?')"
                                        text="Delete" />
                                </form>


                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $courses->links() }}

        </div>
    </div>

    {{-- page specific css --}}
    @push('css')
    @endpush

    {{-- page specific js --}}
    @push('js')
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="{{ asset('ui/backend') }}/js/datatables-simple-demo.js"></script>
    @endpush

</x-backend.layout.master>
