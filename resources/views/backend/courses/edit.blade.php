<x-backend.layout.master>
    <x-slot:title>
        Course Edit
    </x-slot:title>

    @push('css')
        <link href="{{ asset('ui/backend') }}/css/sb-admin-2.min.css" rel="stylesheet">
    @endpush

    @push('js')
    @endpush


    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2>{{ $heading ?? 'Course' }}</h2>
        </div>
        <div class="card-body ">

            <i class="fas fa-table me-1 mb-4"></i>
            {{ $title ?? 'Course Edit' }}
            <a class="btn btn-sm btn-primary" href="">{{ $title ?? 'Edit' }}</a>

            <x-backend.alerts.errors />

            <form method="POST" action="{{ route('courses.update', ['course' => $course->id]) }}"
                enctype="multipart/form-data">
                @csrf
                @method('patch')

                <x-backend.forms.input name="title" type="text" :value="old('title', $course->title)" />
                <x-backend.forms.input name="batch_no" type="text" :value="old('batch_no', $course->batch_no)" />
                <x-backend.forms.input name="class_stard_date" type="date" :value="old('class_stard_date', $course->class_stard_date)" />
                <x-backend.forms.input name="class_end_date" type="date" :value="old('class_end_date', $course->class_end_date)" />
                <x-backend.forms.input name="instructor_name" type="text" :value="old('instructor_name', $course->instructor_name)" />

                <img src="{{ asset('storage/courses/' . $course->image) }}" alt="{{ $course->title }} Image"
                    height="200">
                <x-backend.forms.input name="image" type="file" />
                
                <x-backend.forms.checkbox class="form-check-input" name="is_active" type="checkbox" :value="1" />
                <x-backend.forms.checkbox class="form-check-input" name="course_type" type="checkbox"
                    :value="1" />

                <x-backend.forms.button color="primary" text="Edit" />
            </form>
        </div>
    </div>

</x-backend.layout.master>
