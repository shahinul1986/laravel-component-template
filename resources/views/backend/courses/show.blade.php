<x-backend.layout.master>
    <x-slot:title>
        Course Details
    </x-slot:title>

    @push('css')
        <link href="{{ asset('ui/backend') }}/css/sb-admin-2.min.css" rel="stylesheet">

        <link href="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    @endpush

    @push('js')
        <script src="{{ asset('ui/backend') }}/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <script src="{{ asset('ui/backend') }}/js/demo/datatables-demo.js"></script>
    @endpush

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2>{{ $heading ?? 'Courses' }}</h2>
        </div>
        <div class="card-body">
            <i class="fas fa-table me-1 mb-4"></i>
            Course Show
            <a class="btn btn-sm btn-primary" href="{{ route('courses.index') }}">List</a>
            

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <tr>
                        <th>Title</th>
                        <th>{{$course->title}}</th>
                    </tr>
                    <tr>
                        <th>Batch_No</th>
                        <th>{{$course->batch_no}}</th>
                    </tr>
                    <tr>
                        <th>Class_Start_Date</th>
                        <th>{{$course->class_stard_date}}</th>
                    </tr>
                    <tr>
                        <th>Class_End_Date</th>
                        <th>{{$course->class_end_date}}</th>
                    </tr>
                    <tr>
                        <th>Instructor</th>
                        <th>{{$course->instructor_name}}</th>
                    </tr>
                    <tr>
                        <th>Baner</th>
                        <th>
                            <img src="{{asset('storage/courses/'.$course->image)}}" alt="{{$course->title}} Image" height="200">
                        </th>
                    </tr>
                    
                    <tr>
                        <th>Is Active</th>
                        <th>{{$course->is_active ? 'Active':'In Active'}}</th>
                    </tr>
                    <tr>
                        <th>Course_type</th>
                        <th>{{$course->course_type ? 'Active':'In Active'}}</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>


</x-backend.layout.master>
