<x-backend.layout.master>
    <x-slot:title>
        Category Edit
    </x-slot:title>

    @push('css')
        <link href="{{ asset('ui/backend') }}/css/sb-admin-2.min.css" rel="stylesheet">
    @endpush

    @push('js')
    @endpush


    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2>{{ $heading ?? 'Categories' }}</h2>
        </div>
        <div class="card-body ">

            <i class="fas fa-table me-1 mb-4"></i>
            {{ $title ?? 'Category Edit' }}
            <a class="btn btn-sm btn-primary" href="">{{ $title ?? 'Edit' }}</a>

            <x-backend.alerts.errors />

            <form method="POST" action="{{ route('categories.update', ['category' => $category->id]) }}"
                enctype="multipart/form-data">
                @csrf
                @method('patch')

                <x-backend.forms.input name="title" type="text" :value="old('title', $category->title)" />
                <img src="{{ asset('storage/categories/' . $category->image) }}" alt="{{ $category->title }} Image"
                    height="200">
                <x-backend.forms.input name="image" type="file" />
                <x-backend.forms.textarea name="description" :value="old('description', $category->description)" />
                <x-backend.forms.checkbox class="form-check-input" name="is_active" type="checkbox" :value="1" />

                <x-backend.forms.button color="primary" text="Edit" />
            </form>
        </div>
    </div>

</x-backend.layout.master>
