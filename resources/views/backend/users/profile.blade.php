<x-backend.layout.master>
    <x-slot:title>
        Profile
    </x-slot:title>

    @push('css')
        <link href="{{ asset('ui/backend') }}/css/sb-admin-2.min.css" rel="stylesheet">

        <link href="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    @endpush

    @push('js')
        <script src="{{ asset('ui/backend') }}/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <script src="{{ asset('ui/backend') }}/js/demo/datatables-demo.js"></script>
    @endpush

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2>{{ $heading ?? 'My Profile' }}</h2>
        </div>
        <div class="card-body">
            <i class="fas fa-table me-1 mb-4"></i> {{ $title ?? 'Category List' }}

            <x-backend.alerts.message type="success" :message="session('message')" />

            <div class="table-responsive">
                <table class="table table-bordered"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Name#</th>
                            <th>Email</th>
                            <th>Github URL</th>
                            <th>LinkedIn URL</th>
                            <th>Bio</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <td>{{auth()->user()->name}}</td>
                        <td>{{auth()->user()->email}}</td>
                        <td>{{auth()->user()->profile->github_link}}</td>
                        <td>{{auth()->user()->profile->linkedIn_link}}</td>
                        <td>{{auth()->user()->profile->bio}}</td>

                    </tbody>
                </table>
            </div>
        </div>
    </div>


</x-backend.layout.master>
