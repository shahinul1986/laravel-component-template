<x-backend.layout.master>
    <x-slot:title>
        Users 
    </x-slot:title>

    @push('css')
        <link href="{{ asset('ui/backend') }}/css/sb-admin-2.min.css" rel="stylesheet">

        <link href="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    @endpush

    @push('js')
        <script src="{{ asset('ui/backend') }}/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <script src="{{ asset('ui/backend') }}/js/demo/datatables-demo.js"></script>
    @endpush

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2>{{ $heading ?? 'User' }}</h2>
        </div>
        <div class="card-body">
            <i class="fas fa-table me-1 mb-4"></i> {{ $title ?? 'Category List' }}

            <x-backend.utilities.link-add href="{{ route('users.create') }}" />


            {{-- Error Message --}}
            <x-backend.alerts.message type="success" :message="session('message')" />

            <div class="table-responsive">
                {{-- <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0"> --}}
                <table class="table table-bordered"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>SL#</th>
                            <th>Role Id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->role->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->password }}</td>
                                <td>

                                    <x-backend.utilities.link-show
                                        href="{{ route('users.show', ['user' => $user->id]) }}" />
                                    <x-backend.utilities.link-edit
                                        href="{{ route('users.edit', ['user' => $user->id]) }}" />

                                    <form method="POST"
                                        action="{{ route('users.destroy', ['user' => $user->id]) }}"style="display:inline">
                                        @csrf
                                        @method('delete')

                                        <x-backend.forms.button color="danger" text="Delete"
                                            onclick="return confirm('Are you sure want to delete')" />
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                {{$users->links()}}
            </div>
        </div>
    </div>


</x-backend.layout.master>
