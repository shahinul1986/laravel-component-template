<x-backend.layout.master>
    <x-slot:title>
        colors Edit
    </x-slot:title>

    @push('css')
        <link href="{{ asset('ui/backend') }}/css/sb-admin-2.min.css" rel="stylesheet">
    @endpush

    @push('js')
    @endpush


    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2>{{ $heading ?? 'Colors' }}</h2>
        </div>
        <div class="card-body ">

            <i class="fas fa-table me-1 mb-4"></i>
            {{ $title ?? 'Colors Edit' }}
            <a class="btn btn-sm btn-primary" href="">{{ $title ?? 'Edit' }}</a>

            <x-backend.alerts.errors />

            <form method="POST" action="{{ route('colors.update', ['color' => $color->id]) }}">
                @csrf
                @method('patch')

                <x-backend.forms.input name="title" type="text" :value="old('title', $color->title)" />
                <x-backend.forms.input name="color_code" type="color" :value="old('color_code', $color->color_code)" />



                <x-backend.forms.button color="primary" text="Edit" />
            </form>
        </div>
    </div>

</x-backend.layout.master>
