<x-backend.layout.master>
    <x-slot:title>
        Color Details
    </x-slot:title>

    @push('css')
        <link href="{{ asset('ui/backend') }}/css/sb-admin-2.min.css" rel="stylesheet">

        <link href="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    @endpush

    @push('js')
        <script src="{{ asset('ui/backend') }}/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <script src="{{ asset('ui/backend') }}/js/demo/datatables-demo.js"></script>
    @endpush

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2>{{ $heading ?? 'Colors' }}</h2>
        </div>
        <div class="card-body">
            <i class="fas fa-table me-1 mb-4"></i>
            Category Show
            <a class="btn btn-sm btn-primary" href="{{ route('colors.index') }}">List</a>
            

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                   <tr>
                        <th>Title</th>
                        <th>{{$color->title}}</th>
                    </tr>
                    <tr>
                        <th>Color Name</th>
                        <th>{{$color->color_code}}</th>
                    </tr>
                    
                </table>
            </div>
        </div>
    </div>


</x-backend.layout.master>
