<x-backend.layout.master>
    <x-slot:title>
        Product Size List
    </x-slot:title>

    @push('css')
        <link href="{{ asset('ui/backend') }}/css/sb-admin-2.min.css" rel="stylesheet">

        <link href="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    @endpush

    @push('js')
        <script src="{{ asset('ui/backend') }}/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <script src="{{ asset('ui/backend') }}/js/demo/datatables-demo.js"></script>
    @endpush

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2>{{ $heading ?? 'Sizes' }}</h2>
        </div>
        <div class="card-body">
            <i class="fas fa-table me-1 mb-4"></i> {{ $title ?? 'Sizes List' }}

            <x-backend.utilities.link-add href="{{ route('sizes.create') }}" />

            {{-- Error Message --}}
            <x-backend.alerts.message type="success" :message="session('message')" />

            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>SL#</th>
                            <th>Size Title</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    </thead>
                    </thead>

                    <tbody>
                        @foreach ($sizes as $size)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $size->title }}</td>
                                <td>
                                    <x-backend.utilities.link-show
                                        href="{{ route('sizes.show', ['size' => $size->id]) }}" />
                                    <x-backend.utilities.link-edit
                                        href="{{ route('sizes.edit', ['size' => $size->id]) }}" />

                                    <form method="POST"
                                        action="{{ route('sizes.destroy', ['size' => $size->id]) }}"style="display:inline">
                                        @csrf
                                        @method('delete')

                                        <x-backend.forms.button color="danger" text="Delete"
                                            onclick="return confirm('Are you sure want to delete')" />
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                {{ $sizes->links() }}
            </div>
        </div>
    </div>
</x-backend.layout.master>
