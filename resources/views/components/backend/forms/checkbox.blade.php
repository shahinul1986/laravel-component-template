@props(['name'])
<div class="mb-3 form-check">

    <input name="{{ $name }}" id="{{ $name . 'Input' }}"
        {{ $attributes->merge([
            'class' => 'form-check-input',
        ]) }}>

    <x-backend.forms.label id="{{ $name . 'Input' }}" text="{{ ucfirst($name) }}" />
    <x-backend.forms.error name="{{ $name }}" />
</div>
