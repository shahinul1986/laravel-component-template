@props(['message','type'])
{{-- @dd($attributes) --}}

@if ($message)
<div role="alert" {{$attributes->merge(['class' => 'alert alert-'.$type]) }}>
    {{$message }}
</div> 
@endif

