<?php

use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DistrictController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SizeController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';


Route::middleware('auth')->prefix('/admin')->group(function(){
    Route::get('/profile',[UserController::class,'profile'])->name('users.profile');

    Route::get('/',[DashboardController::class,'index'])->name('admin.index');
    Route::get('/list',[DashboardController::class,'table'])->name('admin.list');
    Route::get('/create',[DashboardController::class,'create'])->name('admin.create');
    Route::get('/edit',[DashboardController::class,'edit'])->name('admin.edit');

    // ========= Product Route ========
    Route::resource('products', ProductController::class);
    // ========= Users Route ========
    Route::resource('users', UserController::class);

// ===========Categories Route=====================

    // Route::get('/categories',[CategoryController::class,'index'])->name('categories.index');
    // Route::get('/categories/create',[CategoryController::class,'create'])->name('categories.create');
    // Route::post('/categories',[CategoryController::class,'store'])->name('categories.store');
    // Route::get('/categories/{category}',[CategoryController::class,'show'])->name('categories.show');
    // Route::get('/categories/{category}/edit',[CategoryController::class,'edit'])->name('categories.edit');
    // Route::patch('/categories/{category}',[CategoryController::class,'update'])->name('categories.update');
    // Route::delete('/categories/{category}',[CategoryController::class,'destroy'])->name('categories.destroy');

    Route::get('/categories/trash',[CategoryController::class,'trash'])->name('categories.trash');
    Route::patch('/categories/trash/{id}',[CategoryController::class,'restore'])->name('categories.restore');
    Route::delete('/categories/trash/{id}',[CategoryController::class,'delete'])->name('categories.delete');

    Route::get('categories/download-pdf', [CategoryController::class, 'downloadPdf'])->name('categories.download_pdf');


    Route::resource('categories',CategoryController::class);

    

// ===========Course Route=====================

    // Route::get('/courses',[CourseController::class,'index'])->name('courses.index');
    // Route::get('/courses/create',[CourseController::class,'create'])->name('courses.create');
    // Route::post('/courses',[CourseController::class,'store'])->name('courses.store');
    // Route::get('/courses/{course}',[CourseController::class,'show'])->name('courses.show');
    // Route::get('/courses/{course}/edit',[CourseController::class,'edit'])->name('courses.edit');
    // Route::patch('/courses/{course}',[CourseController::class,'update'])->name('courses.update');
    // Route::delete('/courses/{course}',[CourseController::class,'destroy'])->name('courses.destroy');

    Route::get('/courses/trash',[CourseController::class,'trash'])->name('courses.trash');
    Route::patch('/courses/trash/{id}',[CourseController::class,'restore'])->name('courses.restore');
    Route::delete('/courses/trash/{id}',[CourseController::class,'delete'])->name('courses.delete');
    Route::resource('/courses', CourseController::class);


    // Route::resource('categories',CategoryController::class);

// ===========District Route ========================
//  Route::get('/districts',[DistrictController::class,'index'])->name('districts.index');
//  Route::get('/districts/create',[DistrictController::class,'create'])->name('districts.create');
//  Route::post('/districts',[DistrictController::class,'store'])->name('districts.store');
//  Route::get('/districts/{district}',[DistrictController::class,'show'])->name('districts.show');
//  Route::get('/districts/{district}/edit',[DistrictController::class,'edit'])->name('districts.edit');
//  Route::patch('/districts/{district}',[DistrictController::class,'update'])->name('districts.update');
//  Route::delete('/districts/{district}',[DistrictController::class,'destroy'])->name('districts.destroy');

 Route::resource('districts',DistrictController::class);
//  ========== Brands Route ============
Route::resource('/brands', BrandController::class);


//============ Colors Route=============
    Route::get('/colors',[ColorController::class,'index'])->name('colors.index');
    Route::get('/colors/create',[ColorController::class,'create'])->name('colors.create');
    Route::post('/colors',[ColorController::class,'store'])->name('colors.store');
    Route::get('/colors/{color}',[ColorController::class,'show'])->name('colors.show');

    Route::get('/colors/{color}/edit',[ColorController::class,'edit'])->name('colors.edit');
    
    Route::patch('/colors/{color}',[ColorController::class,'update'])->name('colors.update');
    Route::delete('/colors/{color}',[ColorController::class,'destroy'])->name('colors.destroy');

    // =============Sizes Route=============
    Route::get('/sizes',[SizeController::class,'index'])->name('sizes.index');
    Route::get('/sizes/create',[SizeController::class,'create'])->name('sizes.create');
    Route::post('/sizes',[SizeController::class,'store'])->name('sizes.store');
    Route::get('/sizes/{size}',[SizeController::class,'show'])->name('sizes.show');
    Route::get('/sizes/{size}/edit',[SizeController::class,'edit'])->name('sizes.edit');
    Route::patch('/sizes/{size}',[SizeController::class,'update'])->name('sizes.update');
    Route::delete('/sizes/{size}',[SizeController::class,'destroy'])->name('sizes.destroy');

    // =============Tags Route=============
    Route::get('/tags',[TagController::class,'index'])->name('tags.index');
    Route::get('/tags/create',[TagController::class,'create'])->name('tags.create');
    Route::post('/tags',[TagController::class,'store'])->name('tags.store');
    Route::get('/tags/{tag}',[TagController::class,'show'])->name('tags.show');
    Route::get('/tags/{tag}/edit',[TagController::class,'edit'])->name('tags.edit');
    Route::patch('/tags/{tag}',[TagController::class,'update'])->name('tags.update');
    Route::delete('/tags/{tag}',[TagController::class,'destroy'])->name('tags.destroy');

});
