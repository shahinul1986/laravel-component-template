<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use phpDocumentor\Reflection\Types\This;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
         
            'title' => $this->faker->sentence(),
            'description' => $this->faker->text(),
            'category_id' => $this->faker->randomDigitNotNull()
            
            // 'remember_token' => Str::random(10),
        ];
    }
}
