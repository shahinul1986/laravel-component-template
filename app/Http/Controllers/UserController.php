<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
   public function index()
   {

       $users = User::latest()->paginate(10);
       
       return view('backend.users.index', compact('users'));
   }

   public function create()
   {
       $users = User::all();
       return view('backend.users.create', compact('users'));
   }

   public function store(Request $request)
   {
       try {
           User::create([
               'name' => $request->name,
               'email' => $request->email,
            //    'role_id' => 1,
               'password' => $request->password,
           ]);
           return redirect()->route('users.index')->withMessage('Successfully Created');
       } catch (QueryException $e) {
           return redirect()->back()->withInput()->withErrors($e->getMessage());
       }
   }

   public function show(User $user)
   {
       return view('backend.users.show', compact('user'));
   }

   public function edit(User $user)
   {

       return view('backend.users.edit', compact('user'));
   }

   public function update(Request $request, User $user)
   {
       try {

           $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'role_id' => 3,
            'password' => $request->password,

           ]);
           return redirect()->route('users.index')->withMessage('Successfully Updated');
       } catch (QueryException $e) {

           return redirect()->back()->withInput()->withErrors($e->getMessage());
       }

   }
   public function destroy(User $user)
   {
       try {

           $user->delete();
           return redirect()->route('users.index')->withMessage('Successfully Deleted');

       } catch (QueryException $e) {
           return redirect()->back()->withErrors($e->getMessage());
       }
   }
}
