<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        // $this->authorize('product-list');

        $products = Product::with('category')->latest()->paginate(10);
        
        return view('backend.products.index', compact('products'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('backend.products.create', compact('categories'));
    }

    public function store(Request $request)
    {
        try {
            Product::create([
                'title' => $request->title,
                'category_id' => 1,
                'description' => $request->description,
            ]);
            return redirect()->route('products.index')->withMessage('Successfully Created');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Product $product)
    {
        return view('backend.products.show', compact('product'));
    }

    public function edit(Product $product)
    {

        return view('backend.products.edit', compact('product'));
    }

    public function update(Request $request, Product $product)
    {
        try {

            $product->update([
                'title' => $request->title,
                'description' => $request->description,

            ]);
            return redirect()->route('products.index')->withMessage('Successfully Updated');
        } catch (QueryException $e) {

            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }
    public function destroy(Product $product)
    {
        try {

            $product->delete();
            return redirect()->route('products.index')->withMessage('Successfully Deleted');

        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
