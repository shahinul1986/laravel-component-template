<?php

namespace App\Http\Controllers;

use App\Http\Requests\CourseRequest;
use App\Models\Course;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CourseController extends Controller
{
    public function index()
    {
        $courses = Course::latest()->paginate();
        return view('backend.courses.index', compact('courses'));
    }

    public function create()
    {
        return view('backend.courses.create');
    }

    public function store(CourseRequest $request)
    {
        if ($file = $request->file('image')) {
            $fileName = date('dmY') . time() . '.' . $file->getClientOriginalExtension();
            $file->move(storage_path('app/public/courses'), $fileName);
        }

        try {
            Course::create([
                'title' => $request->title,
                'batch_no' => $request->batch_no,
                'class_stard_date' => $request->class_stard_date,
                'class_end_date' => $request->class_end_date,
                'instructor_name' => $request->instructor_name,
                'image' => $fileName ?? null,
                'is_active' => $request->is_active ? true : false,
                'course_type' => $request->course_type
            ]);
            return redirect()->route('courses.index')->withMessage('Successfully Created');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Course $course)
    {
        return view('backend.courses.show', compact('course'));
    }

    public function edit(Course $course)
    {
        return view('backend.courses.edit', compact('course'));
    }

    public function update(CourseRequest $request, Course $course)
    {
        try {
            if ($file = $request->file('image')) {
                // unlink(storage_path('app/public/courses/') . $course->image);
                $fileName = date('dmY') . time() . '.' . $file->getClientOriginalExtension();
                $file->move(storage_path('app/public/courses'), $fileName);
            }

            $course->update([
                'title' => $request->title,
                'batch_no' => $request->batch_no,
                'class_stard_date' => $request->class_stard_date,
                'class_end_date' => $request->class_end_date,
                'instructor_name' => $request->instructor_name,
                'image' => $fileName ?? $course->image,
                'is_active' => $request->is_active ? true : false,
                'course_type' => $request->course_type

            ]);
            return redirect()->route('courses.index')->withMessage('Successfully Updated');

        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Course $course)
    {
        try {
            $course->delete();
            return redirect()->route('courses.index')->withMessage('Successfully Deleted');
        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        $courses = Course::onlyTrashed()->paginate(2);
        return view('backend.courses.trash', compact('courses'));
    }

    public function restore($id)
    {
        try {
            $course = Course::onlyTrashed()->whereId($id)->firstOrFail();
            $course->restore();
            return redirect()->back()->withMessage('Successfully Restored !');
        } catch (QueryException $e) {
            Log::error($e->getMessage());
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $course = Course::onlyTrashed()->whereId($id)->firstOrFail();
            $course->forceDelete();
            return redirect()->route('courses.index')->withMessage('Successfully Deleted');
        } catch (QueryException $e) {
            Log::error($e->getMessage());
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
