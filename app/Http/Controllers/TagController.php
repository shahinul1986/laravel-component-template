<?php

namespace App\Http\Controllers;

use App\Http\Requests\TagRequest;
use App\Models\Tag;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index(){
        $tags = Tag::latest()->paginate();
        return view('backend.tags.index',compact('tags'));

    }
    public function create(){
        return view('backend.tags.create');
    }

    public function store(TagRequest $request){
        try{
            Tag::create([
                'title'=>$request->title,
            ]);
            return redirect()->route('tags.index')->withMessage('Successfully Create');
        }catch(QueryException $e){
            return redirect()->withInput()->withErrors($e->getMessage());
        }
    }
    public function show(Tag $tag){
        return view('backend.tags.show',compact('tag'));
    }

    public function edit(Tag $tag){
        return view('backend.tags.edit',compact('tag'));
    }

    public function update(TagRequest $request ,Tag $tag){
        try{
            $tag->update([
                'title' => $request->title,
            ]);
            return redirect()->route('tags.index')->withMessage('Successfully Updated');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Tag $tag){
        try{
            $tag->delete();
            return redirect()->route('tags.index')->withMessage('Successfully Deleted');
        }catch(QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
