<?php

namespace App\Http\Controllers;

use App\Http\Requests\DistrictRequest;
use App\Models\District;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    
    public function index(){
        $districts = District::latest()->paginate();

        return view('backend.districts.index', compact('districts'));
    }

    public function create(){
        return view('backend.districts.create');
    }

    public function store(DistrictRequest $request){
        try{
            District::create([
                'title'=>$request->title,
            ]);
    
            return redirect()->route('districts.index')->with('message', 'Successfully Created');

        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(District $district){
       
        return view('backend.districts.show',compact('district'));
    }

    public function edit(District $district){

        return view('backend.districts.edit',compact('district'));
    }

    public function update(DistrictRequest $request,District $district){

        try{
            $district->update([
                'title'=>$request->title,
            ]);
            return redirect()->route('districts.index')->withMessage('Successfully Updated');

        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
        
    }
    public function destroy(District $district){
        try{
            $district->delete();
            return redirect()->route('districts.index')->with('message','Successfully Deleted');

        }catch(QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
