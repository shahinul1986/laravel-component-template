<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Contracts\Session\Session;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Image;
use PDF;

class CategoryController extends Controller
{
    public function index()
    {
        // $this->authorize('categories-list');
        // $categories = Category::all();
        // $categories = Category::orderBy('id','desc')->get();
        // $categories = Category::latest()->get();
        $categories = Category::latest()->paginate(5);
        // dd($categories);
        return view('backend.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('backend.categories.create');
    }

    public function store(CategoryRequest $request)
    {
        try {
            Category::create([
                'title' => $request->title,
                'description' => $request->description,
                'is_active' => $request->is_active,
                'image' => $this->uploadImage($request->file('image'))
            ]);
            // session()->flash('message', 'Successfully Created');
            return redirect()->route('categories.index')->withMessage('Successfully Created');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    // public function show($categoryId){
    // ======Route Model Binding===========
    public function show(Category $category)
    {
        dd($category->products);
        // dd('showing');
        // =====step-1=========
        // $category = Category::where('id','=', $categoryId)->get();
        // dd($category);
        // ====step-2=========
        // $category = Category::findOrFail($categoryId);
        return view('backend.categories.show', compact('category'));
    }

    // public function edit($categoryId){
    // ======= Model Route Binding ========
    public function edit(Category $category)
    {
        // $category = Category::findOrFail($categoryId);

        return view('backend.categories.edit', compact('category'));

        // dd($category);
    }
    public function update(CategoryRequest $request, Category $category)
    {
        // $category = Category::findOrFail($categoryId);

        try {

            if ($request->file('image')) {
                unlink(storage_path('app/public/categories/') . $category->image);
            }

            $category->update([
                'title' => $request->title,
                'description' => $request->description,
                'is_active' => $request->is_active ?? false,
                'image' => $this->uploadImage($request->file('image'))?? $category->image

            ]);
            return redirect()->route('categories.index')->withMessage('Successfully Updated');
        } catch (QueryException $e) {

            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

        // dd($request->all());
        // dd($categoryId);
        // dd($category);
    }
    public function destroy(Category $category)
    {
        // $category = Category::findOrFail($categoryId);
        try {
            if($category->image && file_exists(storage_path('app/public/categories/'.$category->image))){
                unlink(storage_path('app/public/categories/'.$category->image));
            }

            $category->delete();
            return redirect()->route('categories.index')->withMessage('Successfully Deleted');

        } catch (QueryException $e) {
            Log::error($e->getMessage());
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        $categories = Category::onlyTrashed()->paginate();
        return view('backend.categories.trash', compact('categories'));
    }
    public function restore($id)
    {
        try {
            $category = Category::onlyTrashed()->whereId($id)->firstOrFail();
            $category->restore();
            return redirect()->back()->withMessage('Successfully Restored !');
        } catch (QueryException $e) {
            Log::error($e->getMessage());
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $category = Category::onlyTrashed()->whereId($id)->firstOrFail();
            $category->forceDelete();
            return redirect()->route('categories.index')->withMessage('Successfully Deleted');
        } catch (QueryException $e) {
            Log::error($e->getMessage());
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function uploadImage($file=null)
    {
        if(is_null($file)) return $file;

        $fileName = date('dmY') . time() . '.' . $file->getClientOriginalExtension();
        Image::make($file)
            ->resize(300, 200)
            ->save(storage_path('app/public/categories/' . $fileName));
            return $fileName;
    }

    public function downloadPdf(){
        $categories = Category::all();
        $pdf = PDF::loadView('backend.categories.pdf', compact('categories'));
    return $pdf->download('categories.pdf');
    }
}
