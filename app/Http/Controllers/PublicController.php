<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index(){
        return view('frontend.home');
    }
    public function caregory(){
        return view('frontend.category');
    }
    public function productDetails(){
        return view('frontend.product-details');
    }
    public function viewCard(){
        return view('frontend.view-card');
    }
    public function checkout(){
        return view('frontend.checkout');
    }
    public function placeOrder(){
        return view('frontend.place-order');
    }
}
