<?php

namespace App\Http\Controllers;

use App\Http\Requests\SizeRequest;
use App\Models\Size;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    public function index(){
        $sizes = Size::latest()->paginate();
        return view('backend.sizes.index',compact('sizes'));

    }
    public function create(){
        return view('backend.sizes.create');
    }

    public function store(SizeRequest $request){
        try{
            Size::create([
                'title'=>$request->title,
            ]);
            return redirect()->route('sizes.index')->withMessage('Successfully Create');
        }catch(QueryException $e){
            return redirect()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Size $size){
        return view('backend.sizes.show',compact('size'));
    }

    public function edit(Size $size){
        return view('backend.sizes.edit',compact('size'));
    }

    public function update(SizeRequest $request ,Size $size){
        try{
            $size->update([
                'title' => $request->title,
            ]);
            return redirect()->route('sizes.index')->withMessage('Successfully Updated');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Size $size){
        try{
            $size->delete();
            return redirect()->route('sizes.index')->withMessage('Successfully Deleted');
        }catch(QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
