<?php

namespace App\Http\Controllers;

use App\Models\Color;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    public function index(){

        // $categories = Category::all();
        // $categories = Category::orderBy('id','desc')->get();
        // $categories = Category::latest()->get();
        $colors = Color::latest()->paginate();


        // dd($categories);

        return view('backend.colors.index', compact('colors'));
    }
    
    public function create(){
        return view('backend.colors.create');
    }

    public function store(Request $request){
        try{
            Color::create([
                'title'=>$request->title,
                'color_code'=>$request->color_code,
            ]);
    
            // session()->flash('message', 'Successfully Created');
    
            return redirect()->route('colors.index')->withMessage('Successfully Created');

        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
        
    }

    // public function show($categoryId){
        // ======Route Model Binding===========
    public function show(Color $color){
        // dd('showing');
        // =====step-1=========
        // $category = Category::where('id','=', $categoryId)->get();
        // dd($category);
        // ====step-2=========
        // $category = Category::findOrFail($categoryId);
        return view('backend.colors.show',compact('color'));
    }

    // public function edit($categoryId){
        // ======= Model Route Binding ========
    public function edit(Color $color){
        // $category = Category::findOrFail($categoryId);

        return view('backend.colors.edit',compact('color'));

        // dd($category);
    }
    public function update(Request $request,Color $color){
        // $category = Category::findOrFail($categoryId);

        try{
            $color->update([
                'title'=>$request->title,
                'color_code'=>$request->color_code,
            ]);
            return redirect()->route('colors.index')->withMessage('Successfully Updated');

        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
        
        // dd($request->all());
        // dd($categoryId);
        // dd($category);
    }
    public function destroy(Color $color){
        // $category = Category::findOrFail($categoryId);
        try{
            $color->delete();
            return redirect()->route('colors.index')->withMessage('Successfully Deleted');

        }catch(QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
