<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        return view('backend.home');
    }
    public function table(){
        return view('backend.admin.list');
    }
    public function create(){
        return view('backend.admin.create');
    }
    public function edit(){
        return view('backend.admin.edit');
    }
}
