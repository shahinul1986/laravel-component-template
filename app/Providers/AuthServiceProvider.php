<?php

namespace App\Providers;

use App\Models\User;
use App\Policies\CategoryPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('categories-list',[CategoryPolicy::class,'viewAny']);
        Gate::define('colors-list',[CategoryPolicy::class,'viewAny']);

        // Gate::define('colors-list', function (User $user) {
        //     // dd($user);
        //     return $user->id === 3;
        // });
        // Gate::define('sizes-list', function (User $user) {
        //     // dd($user);
        //     return $user->id === 3;
        // });
    }
}
